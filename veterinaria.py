"""Herencia: crear una clase a partir de otra clase existente 

Taller 11: 
Se tiene una veterinaria y se requiere un programa para gestionar las historias clínicas de los clientes 
1. Definir un programa con una clase llamada historia clínica en donde van a tener las historias clínicas de 3 categorías de clientes: felino, canino y porcino. Cada historia clínica tiene unos campos generales y específicos dependiendo la  categoría del cliente.
Tips: utilizar listas una lista para cada categoría
Entregar el diagrama de clases y el programa en python"""

class HistoriaClinica:

    def __init__ (self,nombre,años,peso,sexo,lugasrDeResidencia,estatura,):

        self.nombre=nombre
        self.años=años
        self.peso=peso
        self.sexo=sexo
        self.lugasrDeResidencia=lugasrDeResidencia
        self.estatura=estatura

class Canino(HistoriaClinica):

    def ladrar(self):
        return ("GUAU")
    def aullar(self):
        return True
    def vacunas(self):
        return True
    def cirugias(self):
        return ("Ninguna")
    def enfermedades(self):
        return ("Ninguna")

class Felino(HistoriaClinica):
    def maullar(self):
        return ("MIAU")
    def uñas_retractiles(self):
        return True
    def vacunas(self):
        return True
    def cirugias(self):
        return ("esterilización")
    def enfermedades(self):
        return ("Ninguna")

class Porcino(HistoriaClinica):
    def hablar(self):
        return ("OINK")
    def presenta_pesuña(self):
        return True
    def vacunas(self):
        return False
    def cirugias(self):
        return ("Ninguna")
    def enfermedades(self):
        return ("Disentería porcina")

miCanino=Canino('lucy',"6 años","4 kilos",'hembra','Cali',"60cm",)
print("Datos del canino: ")
print(miCanino.nombre)
print(miCanino.años)
print(miCanino.peso)
print(miCanino.sexo)
print(miCanino.lugasrDeResidencia)
print(miCanino.estatura)
print("El canino ladra: ",miCanino.ladrar())
print("El canino aulla: ",miCanino.aullar())
print("El canino tiene todas las vacunas: ",miCanino.vacunas())
print("El canino tiene alguna cirugia: ",miCanino.cirugias())
print("El canino tiene alguna enfermedad: ",miCanino.enfermedades())


miFelino=Felino('lulu',"3 años","5 kilos",'hembra','Bogotá',"40 cm")
print()
print("Datos del felino: ")
print(miFelino.nombre)
print(miFelino.años)
print(miFelino.peso)
print(miFelino.sexo)
print(miFelino.lugasrDeResidencia)
print(miFelino.estatura)
print("El felino ladra: ",miFelino.maullar())
print("El felino tiene uñas retractiles: ",miFelino.uñas_retractiles())
print("El felino tiene todas las vacunas: ",miFelino.vacunas())
print("El felino tiene alguna cirugia: ",miFelino.cirugias())
print("El felino tiene alguna enfermedad: ",miFelino.enfermedades())

miPorcino=Porcino('polo',"10 años","16 kilos",'macho','Cartagena',"50 cm")
print()
print("Datos del porcino: ")
print(miPorcino.nombre)
print(miPorcino.años)
print(miPorcino.peso)
print(miPorcino.sexo)
print(miPorcino.lugasrDeResidencia)
print(miPorcino.estatura)
print("El procino ladra: ",miPorcino.hablar())
print("El porcino presenta pesuñas: ",miPorcino.presenta_pesuña())
print("El porcino tiene todas las vacunas: ",miPorcino.vacunas())
print("El porcino tiene alguna cirugia: ",miPorcino.cirugias())
print("El porcino tiene alguna enfermedad: ",miPorcino.enfermedades())